package com.bluetechnology.test.generices;

public interface Generator<T> {
	
	T next();
}
